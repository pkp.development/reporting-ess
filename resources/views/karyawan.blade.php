<?php
   header("Content-type: application/vnd-ms-excel");
   header("Content-Disposition: attachment; filename=absenOrganik.xls");
?>
<table class ="table" border= "1">
    <tr>
        <td>Sim ID</td>
        <td>Nama</td>
        <td>Location</td>
        <td>Division</td>
        <td>Departemen</td>
        <td>Rank</td>
        <td>Posisi</td>
        <td>Tanggal</td>
        <td>Absen In</td>
        <td>Absen Out</td>
        <td>Jarak (M)</td>
        <td>Kategori</td>
    </tr>
    @foreach($data as $p)
		<tr>
			<td>{{ $p->simid }}</td>
			<td>{{ $p->nama }}</td>
			<td>{{ $p->location }}</td>
			<td>{{ $p->dept }}</td>
			<td>{{ $p->division }}</td>

            <td>{{ $p->rank }}</td>
			<td>{{ $p->posisi }}</td>
			<td>{{ $p->cdate }}</td>
			<td>{{ $p->absenIn }}</td>
			<td>{{ $p->absenOut }}</td>
            <td>{{ $p->jarak }}</td>
            <td>{{ $p->sts }}</td>
		</tr>
		@endforeach
</table>