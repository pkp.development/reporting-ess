<?php
   header("Content-type: application/vnd-ms-excel");
   header("Content-Disposition: attachment; filename=absenOrganik.xls");
?>
<table class ="table" border= "1">
    <tr>
        <td>Sim ID</td>
        <td>Nama</td>
        <td>Location</td>
        <td>Division</td>
        <td>Departemen</td>
        <td>Rank</td>
        <td>Posisi</td>
        <td>Tanggal</td>
        <td>Absen In</td>
        <td>Absen Out</td>
        <td>Jarak (M)</td>
        <td>Kategori</td>
        <td>Keterangan</td>
        <td>Jam In (ijin)</td>
        <td>Jam Out (ijin)</td>
        <td>Keterangan (ijin)</td>
        <td>Keterangan (cuti)</td>

        <td>Acc Atasan (koreksi)</td>
        <td>Acc Sdm (koreksi)</td>
        <td>Jam In (koreksi)</td>
        <td>Jam Out (koreksi)</td>
        <td>Keterangan (koreksi)</td>
        <td>tanggal Pengajuan (koreksi)</td>
    </tr>
    @foreach($data as $p)
		<tr>
			<td>{{ $p->simid }}</td>
			<td>{{ $p->nama }}</td>
			<td>{{ $p->location }}</td>
			<td>{{ $p->dept }}</td>
			<td>{{ $p->division }}</td>

            <td>{{ $p->rank }}</td>
			<td>{{ $p->posisi }}</td>
			<td>{{ $p->cdate }}</td>
			<td>{{ $p->absenIn }}</td>
			<td>{{ $p->absenOut }}</td>
            <td>{{ $p->jarak }}</td>
            <td>{{ $p->sts }}</td>

            <td>{{ $p->ket }}</td>
            <td>{{ $p->jamIn_ijin }}</td>
            <td>{{ $p->jamOut_ijin }}</td>

            <td>{{ $p->ket_ijin }}</td>
            <td>{{ $p->ket_cuti }}</td>


            <td>{{ $p->approved_atasan}}</td>
            <td>{{ $p->approved_sdm }}</td>
            <td>{{ $p->jam_in_koreksi }}</td>
            <td>{{ $p->jam_out_koreksi }}</td>
            <td>{{ $p->keterangan_koreksi }}</td>
            <td>{{ $p->tanggal_pengajuan_koreksi }}</td>
		</tr>
		@endforeach
</table>