<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('items', function() {
    return[
        'message' => 'ini kamu',
    ];
});
route::get('test', 'BprController@cobaApi');
route::get('dashboard/{id}', 'BprController@dashboard');
route::get('dataKaryawan/{id}', 'BprController@dataKarywan');
route::get('bank/{id}', 'BprController@bank');
route::get('penempatan/{id}', 'BprController@penempatan');
route::get('histori_contract/{id}', 'BprController@contract');
route::get('informasi_keluarga/{id}', 'BprController@informasi_keluarga');
route::get('benefit/{id}', 'BprController@benefit');
Route::get('image/{id}', 'BprController@foto');
Route::get('Cabang', 'BprController@Cabang');
Route::get('branchSim', 'BprController@branchSim');
Route::get('companySim', 'BprController@companySim');
Route::post('coba', 'BprController@tesP');
Route::get('propinsi', 'BprController@propinsi');
Route::get('kota', 'BprController@kota');
Route::get('agama', 'BprController@agama');
Route::get('masterbank', 'BprController@masterBank');
// Route::get('credensialAmbil', 'BprController@credensialAmbil');
// Route::post('credensialStatus', 'BprController@credensialStatus');
Route::get('pro', 'BprController@pro');
Route::get('CabangClient', 'BprController@CabangClient');
Route::get('lastContract/{id}', 'BprController@DetailContract');
Route::get('checkStatus/{id}', 'BprController@CheckStatus');
Route::get('historiContract/{id}', 'BprController@historiContract');
Route::get('plafon/{id}', 'BprController@Plafon');
Route::get('plafon2/{id}', 'BprController@Plafon2');
Route::get('plafon3/{id}', 'BprController@Plafon3');
Route::get('GajianDuluan/{id}', 'BprController@Gapok');
Route::get('credensialKetinggalan/{id}', 'BprController@credensialKetinggalan');
route::get('penempatan2/{id}', 'BprController@penempatan2');
Route::get('lastContract2/{id}', 'BprController@DetailContract2');
Route::post('authPkp', 'BprController@authPkp');
Route::get('dataUserPkp', 'BprController@dataUserPkp');
Route::get('GajianDuluanbeta/{id}', 'BprController@Gapokbeta');
//------------------
Route::post('gaweGp', 'BprController@gawe');
Route::get('datasrk/{id}', 'BprController@datasrk');

Route::post('vnInsertPkp', 'BprController@vnInsertPkp');
Route::post('vnGetPkp', 'BprController@vnGetPkp');
Route::post('vnInsertOrder', 'BprController@vnInsertOrder');
Route::post('vnFlag', 'BprController@vnFlag');
Route::post('vnFlag', 'BprController@vnFlag');
Route::post('vnGetRow', 'BprController@vnGetRow');
Route::get('GaweStg/{id}', 'BprController@GaweStg');
Route::get('vnGetHis', 'BprController@GetHis');
Route::post('vnGetPkpJobId', 'BprController@vnGetJobId');

// Route::get('CredensialAwalUat/{id}', 'BprController@CredensialAwalUat');
// Route::post('credensialStatusUat', 'BprController@credensialStatusUat');

Route::get('credensialAmbil', 'BprController@CredensialAwalUat');
Route::post('credensialStatus', 'BprController@credensialStatusUat');

Route::get('testcr/{id}', 'BprController@testcr');


