<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::get('halo', 'DepanController@pertama');

route::get('absenOrganik', 'DepanController@tampil');
//route::post('/pegawai/store', 'DepanController@pertama');
route::post('/pegawai/store','DepanController@pertama');

route::get('tampilcss/321123000xxAz!', 'DepanController@tampilCss');

route::get('uploadDakon', 'DepanController@uploadDakon');

route::post('/uploadData','DepanController@uploadData');

route::get('hai', 'DepanController@cek');
