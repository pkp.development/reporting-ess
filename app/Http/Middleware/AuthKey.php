<?php

namespace App\Http\Middleware;

use Closure;

class AuthKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $token =$request->header('api-token');
        // $tgl  = date('dmY');
        // $key  = 'SIM@2021';
        // $token_in = $key . $tgl ;
        // $token_in = sha1($token_in);
        // if($token !=  $token_in){
        //     return response()->json(['message'=>'token not match'],401);
        // }
        return $next($request);
    }
}
