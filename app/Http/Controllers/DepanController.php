<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class DepanController extends BaseController
{

    public function cek(){
        echo "hai" ;
    }

    public function tampil(){
        return view('tampil');
    }

    public function filter(Request $request){
        $tgl1  = $request->tanggal1 ;
        $tgl2  = $request->tanggal2 ;
        $data = explode("-" , $tgl1);
        $tahun1  = $data[0];
        $bulan1  = $data[1];
        $tgl1    = $data[2];
        $tanggal1 = $tgl1 . "/" . $bulan1 . "/" . $tahun1 ;

        $data2 = explode("-" , $tgl2);
        $tahun2  = $data2[0];
        $bulan2  = $data2[1];
        $tgl2    = $data2[2];
        $tanggal2 = $tgl2 . "/" . $bulan2 . "/" . $tahun2 ;

        echo $tanggal1  . "-" . $tanggal2 ; 
        die();
    }

    public function pertama(Request $request){
        $tgl1  = $request->tanggal1 ;
        $tgl2  = $request->tanggal2 ;
        ini_set('max_execution_time', '0');
        //DB::connection('mysql2')->table('absen_organik')->delete();

        $data = DB::connection('mysql2')->table('absen_organik')->get();
       return view('karyawan2',['data' => $data]);
        die() ;
        //  Stop 
        //DB::connection('mysql2')->table('absen_organik')->delete();
        // echo "hapus";
        // die();
        $pegawai     = DB::table('emp')
                      ->where('type','5464')
                      //->where('id','<>','100100389')
                      ->orderBy('id')
                      ->get();

        $jum         = count($pegawai);    
        $jum         = $jum - 1 ; 
        
        $jum        = 394 ; 
        // echo "<pre>" ;
        // print_r($jum) ; //394
        // die() ;
        // $jum   = 200 ;
        // echo $jum  ;
      // die() ;
        //$x = 50 ; 
        for ($x = 300 ; $x <= $jum; $x++) { 
            echo $x . $pegawai[$x]->name . "<br>" ; 
            $nama  = $pegawai[$x]->name ;
            $idPegawai  = $pegawai[$x]->id ;
            $simId      = $pegawai[$x]->reg_no ;
            $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$idPegawai)
                                     ->where('status','1')->orderByDesc('id')->first();
            $branchSim             = DB::table('bisnis_network')->where('id', $pegawai_phist->network_id)->first();
            // echo  "nama karyawan : " .  $nama . "network id : " . $pegawai_phist->network_id . "--" ;
            // echo  "branch sim : " .  $branchSim->id ." branch name : " .  $branchSim->cabang ."<br>" ; 
            if ($branchSim == NULL){
                $branchId              = "64"; 
                $branchNama            = "0"  ; 
            }else{
                $branchId              = $branchSim->id ; 
                $branchNama            = $branchSim->cabang  ; 
            }
            $posisi                = $pegawai_phist->pos_name ;
            $rankId                = $pegawai_phist->rank ;
           // echo $simId .  "xx". $rankId . "<br>" ; 
            $rank    = DB::table('mst_data')->where('kodeData',$rankId)->first();
           // $rankNm  =  $rank->namaData ;
            $rankNm  =  (isset($rank->namaData ) ? $rank->namaData  : null) ;
            //(isset($emp_bank->account_no ) ? $emp_bank->account_no  : null) ;
          //  echo $branchId . "-" ;
          // $latBranch = (isset($emp_bank->bank_id ) ?$emp_bank->bank_id  : null) ;
            $branchKordinat        = DB::table('abon_absent_locations')->where('company_branch_id', $branchId)->first();
            $latBranch  = (isset($branchKordinat->latitude) ?$branchKordinat->latitude  : 0) ;
            $longBranch = (isset($branchKordinat->longitude) ?$branchKordinat->longitude  : 0) ;
        
            
            $absenHarian        = DB::table('att_absen')->where('pegawai_id', $idPegawai)
                                                        ->whereBetween('tanggal_mulai', [$tgl1, $tgl2])->get();
            $jumlah             = count($absenHarian) ; 
            $jumlah             = $jumlah -  1 ; 
            $jdt                = count($absenHarian) ;

            if ($jdt  > 0 ){
                // echo " x jumlah absenyy x " .  $jdt ; 
                // die() ;
              
                for ($y = 0; $y <= $jumlah; $y++) { 
                   // echo "-disini-" ; 
                    // att hadir 
                    $absenHadir       = DB::table('att_hadir')->where('pegawai_id', $idPegawai)
                                                              ->where('tanggal', $absenHarian[$y]->tanggal)
                                                              ->first();
                    if ($absenHadir == NULL){
                        $ijinIn    = ""; 
                        $ijinOut   = "" ; 
                        $ketIjin  ="" ; 
                    }else{
                        $ijinIn    = $absenHadir->mulai ; 
                        $ijinOut   = $absenHadir->selesai ; 
                        $ketIjin   = $absenHadir->keterangan ; 
                    }  
                    
                    //cuti
                    $absencuti       = DB::table('att_cuti')->where('pegawai_id', $idPegawai)
                                                              ->where('tanggal_mulai', $absenHarian[$y]->tanggal_mulai)
                                                              ->first();
                

                    if ($absencuti == NULL){ 
                       $ketCuti  ="" ; 
                    }else{
                      
                        $ketCuti   = $absencuti->keterangan ; 
                       
                    }  
                    
                    //------------------------------------------------
                    // koreksi 
                $absenkoreksi       = DB::table('att_koreksi')->where('pegawai_id', $idPegawai)
                                  ->where('tanggal_mulai', $absenHarian[$y]->tanggal_mulai)
                                  ->first();
                
                if ($absenkoreksi  == NULL){ 
                    $ketKoreksi           = "" ; 
                    $jamIn_koreksi        = "" ; 
                    $jamOut_koreksi       = "" ;
                    $tglPengajuan_koreksi = "" ;
                    $apprAtasan_koreksi   = "" ; 
                    $appSdm_koreksi       = "" ;

                }else{
                    $ketKoreksi           = $absenkoreksi->keterangan ;   
                    $jamIn_koreksi        = $absenkoreksi->koreksi_mulai ;  
                    $jamOut_koreksi       = $absenkoreksi->koreksi_selesai ; 
                    $tglPengajuan_koreksi = $absenkoreksi->tanggal ;
                    $apprAtasan_koreksi   = $absenkoreksi->atasan_status ;
                    $appSdm_koreksi       = $absenkoreksi->sdm_status ;               
                 }  


                    $ket   = $absenHarian[$y]->keterangan ; 
                    if ($absenHarian[$y]->keterangan == 'mobile'){
                        $ket   = "absen";
                    }
                   // $tanggal =  $absenHarian[$y]->tanggal ;
                    $tanggal =  $absenHarian[$y]->tanggal_mulai ;
                    $lat1  = $absenHarian[$y]->mulai_latitude; 
                    $lon1  = $absenHarian[$y]->mulai_longitude; 
                    $lat2  = $latBranch ;
                    $lon2  = $longBranch ;
                    $unit  = "K" ; 
                    
                    $theta = $lon1 - $lon2;
                    $dist  = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                    $dist  = acos($dist);
                    $dist  = rad2deg($dist);
                    $miles = $dist * 60 * 1.1515;
                    $unit  = strtoupper($unit);
                    $jarak = $miles * 1.609344;
    
                     $jarak = $jarak * 1000 ; 
                     $data  = explode("." , $jarak);
                     $jarak = $data[0];
                     if ($jarak > 150 ){
                        $sts  = "WFH" ;
                     }else {
                        $sts   = "WFO" ;
                     }
                        
                        DB::connection('mysql2')->table('absen_organik')->insert([
                        'simId'    => $simId ,
                        'nama'     => $nama,
                        'cdate'    => $tanggal,
                        'cabang'   => $branchNama,
                        'idCabang'      => $branchId ,
                        'jarak'         => $jarak,
                        'longbranch'    => $longBranch,
                        'latbranch'     => $branchNama,
                        'latabsen'      => $lat1 ,
                        'longabsen'     => $lon1,
                        'location'      => $branchNama,
                        'division'      =>  '0',    
                        'dept'         => '0' ,
                        'rank'         => $rankNm,
                        'posisi'       => $posisi,
                        'absenIn'      => $absenHarian[$y]->mulai,
                        'absenOut'     => $absenHarian[$y]->selesai,
                        'sts'          => $sts,
                        'ket'          => $ket, 
    
                        'jamIn_ijin'   => $ijinIn,
                        'jamOut_ijin'  => $ijinOut,
                        'ket_ijin'      => $ketIjin ,
                        'ket_cuti'     => $ketCuti , 
    
                        'jam_in_koreksi'              => $jamIn_koreksi,
                        'jam_out_koreksi'             => $jamOut_koreksi, 
                        'tanggal_pengajuan_koreksi'   => $tglPengajuan_koreksi,
                        'approved_atasan'             => $apprAtasan_koreksi,
                        'approved_sdm'                => $appSdm_koreksi ,
                        'keterangan_koreksi'          => $ketKoreksi , 
                    ]);
                     
    
                }
            }

            
          
        }

        $data = DB::connection('mysql2')->table('absen_organik')->get();
        return view('karyawan2',['data' => $data]);
        
    }

    public function tampilcss(){

       // $data = DB::connection('mysql2')->table('user_dakon')->get(); 
       // return view('tampil');
    }


    public function uploadDakon(){
        // $pegawai     =DB::connection('mysql4')->table('user_dakon')->get();
        // echo "<pre>" ;
        // print_r($pegawai);
        // die() ;
        return view('tampildakon');
    }

    public function uploadData(Request $request){
        $fileName = $_FILES["file"]["tmp_name"];
        echo"<pre>" ;
        print_r($fileName);
        if ($_FILES["file"]["size"] > 0) {

            $file = fopen($fileName, "r");
            $id = NULL ;
            while (($column = fgetcsv($file, 10000, ";")) !== FALSE) {
            
                if($column[0]<>"USERNAME"){
                    echo $column[0] . "<br>" ;
                    $pegawai     =DB::connection('mysql4')->table('user_dakon')
                                  ->where('email',$column[1])
                                  ->where('user',$column[0])
                                  ->first();
                    if($pegawai){

                    }else{
                        DB::connection('mysql4')->table('user_dakon')->insert([
                        'user'     => $column[0] ,
                        'nama'     => $column[2],
                        'email'    => $column[1],
                        'id_user'  => '00',
                        'job_id'   => '00',
                        'network_id' => '00',
                        ]); 
                    }  
                    
                    
                    
                }
                
               
            }
        }

    }




   
}