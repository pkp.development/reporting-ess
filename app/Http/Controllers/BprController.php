<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class BprController extends BaseController
{

    public function tesP(Request $request){
        $id  = $request->id ;
        echo "balikannya adalah  : " .  $id ;
    }

    public function foto($fileName){
        $path = public_path().'/images/1.png';
        return Response::download($path);        
    }

    public function index(){
        echo "hai";
        $pegawai = DB::table('emp')->first();
        print_r($pegawai);
    }

    public function cobaApi(){
        $pegawai = DB::table('emp')->first();
        $tes  = "hai";
        return  json_encode($pegawai);

    }

    public function dashboard($id){
        $pegawai      = DB::table('emp')->where('reg_no',$id)->first();
        if ($pegawai == NULL){
             $data  =array(
                'sim_id'       => '',
                'nama_pegawai' => '',
                'npwp'         => '',            
                );
            return  json_encode($data);
            die();
        }
        $data  =array(
                       'sim_id'       => $id,
                       'nama_pegawai' => $pegawai->name,
                       'npwp'         => $pegawai->npwp_no,            
        );
        return  json_encode($data);
    }

    public function dataKarywan($id){
        $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
        if ($pegawai == null){
            $data  =array(
                'sim_id'          => '',
                'npo'             => '',
                'nama_perusahaan' => '',
                'nama_pegawai'    => '',
                'tempat_lahir'    => '',
                'tanggal lahir'   => '',
                'jenis_kelamin'   => '',
                'no_ktp'          => '',
                'npwp'            => '',
                'tahun_terbit_npwp'  => '',
                'alamat_ktp'      => '',
                'provinsi'        => '',
                'kota'            => '',
                'agama'           => '',  
                'telephone 1'     => '',
                'telephone 2'     => '',
                'email'           => '',
                'alamat_domisili' => '',
                'provinsi_domisili' => '',
                'kota_domisili'     => '',
                'foto ktp'        => '',
                'foto profile'    => '' , 
                'company_sim_id'  => '',
                'client_id'       => '',
                'branch_id'       => '',
                'branch_nama'     => '',
      
              );
        }else{
        $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai->id)->first();
        $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->where('status','1')->orderByDesc('id')->first();
        $client_data           = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
        $agama                 = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
        $branchSim             = DB::table('bisnis_network')->where('id', $pegawai_phist->network_id)->first();
        $ktpFile               = $pegawai->ktp_filename ; 
        $picFile               = $pegawai->pic_filename ; 
        if ($ktpFile == NULL){$ktpFile = ""; }
      
        $ktpUrl                = "https://sysbpr.simgroup.co.id/intranet//storage/emp/ktp/";
        $ktpUrl                = $ktpUrl . $ktpFile  ;

        $picUrl                = "https://sysbpr.simgroup.co.id/intranet//storage/recruit/pic/";
        $picUrl                = $picUrl . $picFile  ;
        $data  =array(
                      'sim_id'          => $id,
                      'npo'             => $pegawai_contract->npo_no,
                      'nama_perusahaan' => $client_data->nama,
                      'nama_pegawai'    => $pegawai->name,
                      'tempat_lahir'    => $pegawai->birth_place,
                      'tanggal lahir'   => $pegawai->birth_date,
                      'jenis_kelamin'   => $pegawai->gender,
                      'no_ktp'          => $pegawai->ktp_no,
                      'npwp'            => $pegawai->npwp_no,
                      'tahun_terbit_npwp'  => $pegawai->npwp_date,
                      'alamat_ktp'      => $pegawai->ktp_address,
                      'provinsi'        => $pegawai->ktp_prov,
                      'kota'            => $pegawai->ktp_city,
                      'agama'           => $pegawai->religion,  
                      'telephone 1'     => $pegawai->phone_no,
                      'telephone 2'     => $pegawai->cell_no,
                      'email'           => $pegawai->email,
                      'alamat_domisili' => $pegawai->dom_address,
                      'provinsi_domisili' => $pegawai->dom_prov,
                      'kota_domisili'     => $pegawai->dom_city,
                      'foto ktp'        => $ktpUrl,
                      'foto profile'    => $picUrl , 
                      'company_sim_id'  => $pegawai_phist->business_id,
                      'client_id'       => $pegawai_phist->client_id,
                      'branch_id'       => $branchSim->id,
                      'branch_nama'     => $branchSim->cabang,
            
                    );
        }
        
        return  json_encode($data);
    }

    public function bank($id){
        $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
        if ($pegawai == NULL){
            $data               = array(
                'nama_bank' => '',
                'no_rek'    => '',
                'nama_pemilik_rekening' => '',
                );
                echo json_encode($data);
                exit();
        }else{
            $pegawai_bank          = DB::table('emp_bank')->where('parent_id',$pegawai->id)->first();
            if ($pegawai_bank == NULL){
                    $data               = array(
                    'nama_bank' => '',
                    'no_rek'    => '',
                    'nama_pemilik_rekening' => '',
                    );
                    echo json_encode($data);
                    exit();
            }
            
            $nama_bank             = DB::table('mst_data')->where('kodeData',$pegawai_bank->bank_id)->first();
            $data               = array(
                                'nama_bank' => $nama_bank->namaData,
                                'no_rek'    => $pegawai_bank->account_no,
                                'nama_pemilik_rekening' => $pegawai_bank->account_name,
            );
        }
       

        echo json_encode($data);

    }

    public function penempatan($id){
        $pegawai             = DB::table('emp')->where('reg_no',$id)->first();
        $pegawai_client      = DB::table('emp_phist')->where('parent_id',$pegawai->id)->where('status','1')->first();
        $client              = DB::table('client_data')->where('id',$pegawai_client->client_id)->first();
        //$cabang              = DB::table('client_address')->where('id_address',$pegawai_client->area_id)->first();
        $contract            = DB::table('emp_contract')->where('parent_id',$pegawai->id)->orderBy('id', 'asc')->first();
        $job                 = DB::table('rec_job_posisi')->where('id',$contract->job)->first();
        $pegawai_exit        = DB::table('emp_exit_termination')->where('id_pegawai',$pegawai->id)->first();
        $contract_his        = DB::table('emp_contract')->select('npo_no','pos_name','job','start_date','end_date')->where('parent_id',$pegawai->id)->orderBy('id', 'asc')->get();
        $cabang              = DB::table('client_address')->where('id_address',$pegawai_client->area_id)->first();
        if($cabang == NULL){
            $cabang              = DB::table('client_address')->where('id_address',$pegawai_client->area_id)->first();
         }
        
        if ($contract->job == "0"){
            $job_id     ='';
            $job_nama   ='';
        }else{
            $job_id     = $job->id;
            $job_nama   = $job->subject;
        }

        if ($pegawai_exit <> NULL){
            $srk_file            =  $pegawai_exit->srk_generate_file ;
            $skk_file            =  $pegawai_exit->skk_generate_file ;
            $srkPath             = "https://sysbpr.simgroup.co.id/intranet//storage/exit_termination/srk/" .  $srk_file;
            $srkPath             = $srk_file  ;
            $skkPath             = "https://sysbpr.simgroup.co.id/intranet//storage/exit_termination/skk/" . $skk_file;
        }else{
            $srkPath             = "";
            $skkPath             = "";
        }
      


        // echo  $srkPath . "-" . $skkPath ;
        // die();
        $data      = array(
                            'id_klien'   =>$client->id,
                            'nama_klien' =>$client->nama,
                            'id_cabang'  =>$cabang ->id_address,
                            'cabang'     =>$cabang ->nama_bagian,
                            'id_job'     =>$job_id,
                            'nama_job'   =>$job_nama,
                            'join_date'  =>$pegawai->join_date,
                            'start_date' =>$contract->start_date,
                            'end_date'   =>$contract->end_date,
                            'skk'        =>$skkPath,
                            'srk'        =>$srkPath,

                            
                          );
        $jum = count($contract_his);

        for ($i = 0; $i < $jum; $i++){
            if ($contract_his[$i]->pos_name == null){
                $nmPsi   = "kosong";
            }else{
                $nmPsi  = $contract_his[$i]->pos_name ;
            }
            $tambah= array('nama_posisi'=> $nmPsi,'stat date' => $contract_his[$i]->start_date,'end date' => $contract_his[$i]->end_date );

            $data["detail"][] = $tambah;
        }                  
        return json_encode($data);
    }

    public function contract($id){
        $pegawai             = DB::table('emp')->where('reg_no',$id)->first();
        $contract            = DB::table('emp_contract')->select('npo_no','pos_name','job','start_date','end_date')->where('parent_id',$pegawai->id)->orderBy('id', 'asc')->get();
        return  json_encode($contract);

    }

    public function informasi_keluarga($id){
        $pegawai             = DB::table('emp')->where('reg_no',$id)->first();
        if ($pegawai  ==NULL){
            $data = array(
                           'status' =>'500',
                           'message'=>'Data Tidak Ditemukan'
                        );
        }else{
            $status_nikah        = DB::table('mst_data')->where('kodeData',$pegawai->marital)->first();
            $pegawai_fm          = DB::table('emp_family')->where('parent_id',$pegawai->id)->get();
            $pegawai_kk          = DB::table('rec_applicant')->where('ktp_no',$pegawai->ktp_no)->first();
            $jum  =  count($pegawai_fm);
            if ($status_nikah == NULL){
                $sts_nikah = '';
            }else{
                $sts_nikah = $status_nikah->namaData ;
            }
            //(isset($pegawai_contract->job) ?$pegawai_contract->job  : null); $pegawai_kk->kk_no
            $data   = array('matrial status' => $sts_nikah,'no kk' => (isset($pegawai_kk->kk_no) ?$pegawai_kk->kk_no  : null)) ;
            for ($i = 0; $i < $jum; $i++){
               $tempat_lahir        = DB::table('mst_data')->where('kodeData', $pegawai_fm[$i]->birth_place)->first();
               $hubungan            = DB::table('mst_data')->where('kodeData', $pegawai_fm[$i]->rel)->first();

                $tambah= array('nama'=> $pegawai_fm[$i]->name,'tempat_lahir' => $tempat_lahir->namaData,'tanggal lahir' => $pegawai_fm[$i]->birth_date,
                'ktp_no'  => $pegawai_fm[$i]->ktp_no,'gender' => $pegawai_fm[$i]->gender, 'hubungan' => $hubungan ->namaData);
               $data["detail"][] = $tambah;
        }
        }
        
        return  json_encode($data);

    }

    public function benefit($id){
        $pegawai             = DB::table('emp')->where('reg_no',$id)->first();
        $data  = array(
                 'benefit'  => '0'

        );
        return  json_encode($data);

    }

    public function propinsi(){
        $propinsi    = DB::table('mst_data')->select('kodeData','namaData')->where('kodeCategory','S02')->paginate(15);
        return  json_encode($propinsi);
    }

   

    public function agama(){
        $agama    = DB::table('mst_data')->select('kodeData','kodeInduk','namaData')->where('kodeCategory','EAG')->get();
        return  json_encode($agama);
    }

    public function masterBank(){
        $bank    = DB::table('mst_data')->select('kodeData','kodeInduk','namaData')->where('kodeCategory','EBA')->paginate(15);
        return  json_encode($bank);
    }

    public function credensialAmbil(){

        echo "kamu" ; 
        die() ;
        $tad_baru  = DB::connection('mysql2')->table('tad_baru')->first();
        $tad_akhir = $tad_baru->count ;
        
       // $tad_akhir++
        $pegawai  = DB::table('emp')->select('id','reg_no','name','birth_date','phone_no','cell_no','email')
                                    ->where('id','>=',$tad_akhir)
                                    ->where('type','<>','5464')
                                    ->where('status','197861')->first();
        // echo "<pre>" ;
        // print_r($pegawai);
        // echo "</pre>" ;
        // die();
        if($pegawai==NULL){
            $data      = array(
                'status'   =>'404',
                'message' =>'tidak ada tad baru',
            );
        }else{
            $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->first();
            $client_data           = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
            if($client_data ==NULL){
                $companyId   = '';
                $companyName = '';
            }else{
               
                $companyId   = $client_data->id ;
                $companyName = $client_data->nama ;
            }
            $data      = array(
                'status'   =>'200',
                'message'  =>'ada tad baru',
                'sim_id'   =>$pegawai->reg_no,
                'nama'     =>$pegawai->name,
                'birth_date' =>$pegawai->birth_date,
                'phone1'     =>$pegawai->phone_no,
                'phone2'     =>$pegawai->cell_no,
                'email'      =>$pegawai->email,
                'count'      =>$pegawai->id,
                'client_company_id'            => $companyId,
                'client_company_name'          => $companyName,
            );
        }
        DB::connection('mysql2')->table('tad_baru')->where('id','1')->update([
            'count' => $pegawai->id
        ]);
        return  json_encode($data);          
    }

    public function credensialStatus(Request $request){
        $id  = $request->id ;
        $tad_baru = DB::connection('mysql2')->table('tad_baru')->where('count',$id)->first();
        
        if ($tad_baru==NULL){
            $data      = array(
                'status'   =>'404',
                'message' =>'ID Tidak Ditemukan',
            );
        }else{
            $idAkhir  = $tad_baru->count;
            $idAkhir++;
            $data      = array(
                'status'   =>'200',
                'message' =>'Data Berhasil Di update',
                'lastCount'  => $idAkhir ,
            );
            DB::connection('mysql2')->table('tad_baru')->where('id','1')->update([
                'count' => $idAkhir
            ]);
        }

        return  json_encode($data);
    }

    public function pro(Request $req){
        $jumId = strlen($req->id);
        $jumNama  = strlen($req->nama);
        $nama  = trim($req->nama)  ;
        $id    = trim($req->id);
        if ($jumNama > 0)  {
            $propinsi    = DB::table('mst_data')->select('kodeData','namaData')->where('kodeCategory','S02')->where('namaData', 'like', '%' .$nama. '%')->get();
        }
        else if ($jumId > 0)  {
            $propinsi    = DB::table('mst_data')->select('kodeData','namaData')->where('kodeCategory','S02')->where('kodeData', 'like', '%' .$id. '%')->get();
            
        }
        else if ($jumId == "0" and $jumNama == "0"){
            $propinsi    = DB::table('mst_data')->select('kodeData','namaData')->where('kodeCategory','S02')->paginate(15);
           
        }
        return  json_encode($propinsi);
    }

    public function kota(Request $req){
        $jumId = strlen($req->id);
        $jumNama  = strlen($req->nama);
        $jumPro  = strlen($req->idPro);
        $nama  = trim($req->nama)  ;
        $id    = trim($req->id);
        $kodeInduk = trim($req->idPro);
        if ($jumNama > 0)  {
            $kota    = DB::table('mst_data')->select('kodeData','kodeInduk','namaData')->where('kodeCategory','S03')->where('namaData', 'like', '%' .$nama. '%')->get();
        }
        else if ($jumId > 0)  {
            $kota    = DB::table('mst_data')->select('kodeData','kodeInduk','namaData')->where('kodeCategory','S03')->where('kodeData', 'like', '%' .$id. '%')->get();
        }else if ( $jumPro > 0)  {
            $kota    = DB::table('mst_data')->select('kodeData','kodeInduk','namaData')->where('kodeCategory','S03')->where('kodeInduk', 'like', '%' .$kodeInduk. '%')->get();
        }else {
            $kota    = DB::table('mst_data')->select('kodeData','kodeInduk','namaData')->where('kodeCategory','S03')->paginate(15);   
        }
        
        return  json_encode($kota );
    }

    public function companySim(Request $req){
        $jumId = strlen($req->id);
        $jumNama  = strlen($req->nama);
        $nama  = trim($req->nama)  ;
        $id    = trim($req->id);
        if ($jumNama > 0)  {
            $companySim    = DB::table('bisnis_grup')->select('id','perusahaan','kode','alamat')->where('perusahaan', 'like', '%' .$nama. '%')->get();  
        }
        else if ($jumId > 0)  {
            $companySim    = DB::table('bisnis_grup')->select('id','perusahaan','kode','alamat')->where('id', 'like', '%' .$id. '%')->get();  
        }else {
            $companySim    = DB::table('bisnis_grup')->select('id','perusahaan','kode','alamat')->paginate(15);  
        }
       
        return  json_encode($companySim);
    }

    public function branchSim(Request $req){
        $jumId = strlen($req->id);
        $jumNama  = strlen($req->nama);
        $jumIdP  = strlen($req->id_perusahaan);
        $nama  = trim($req->nama)  ;
        $id    = trim($req->id);
        $idP    = trim($req->id_perusahaan);
        if ($jumNama > 0)  {  
            $branchSim    = DB::table('bisnis_network')->select('id','id_perusahaan','cabang')->where('cabang', 'like', '%' .$nama. '%')->get(); 
        }else if ($jumId > 0)  { 
            $branchSim    = DB::table('bisnis_network')->select('id','id_perusahaan','cabang')->where('id',$id)->get(); 
        }else if ($jumIdP > 0)  { 
            $branchSim    = DB::table('bisnis_network')->select('id','id_perusahaan','cabang')->where('id_perusahaan',$idP)->get(); 
        }else {    
            $branchSim    = DB::table('bisnis_network')->select('id','id_perusahaan','cabang')->orderBy('id', 'asc')->paginate(15);  
        }
        return  json_encode($branchSim);
    }

    public function Cabang(Request $req){
        $jumId = strlen($req->id);
        $jumNama  = strlen($req->nama);
        $nama  = trim($req->nama)  ;
        $id    = trim($req->id);
        if ($jumNama > 0)  { 
            $clientCabang    = DB::table('client_data')->select('id','nama')->where('nama', 'like', '%' .$nama. '%')->get();  
        }
        else if ($jumId > 0)  { 
            $clientCabang    = DB::table('client_data')->select('id','nama')->where('id', 'like', '%' .$id. '%')->get();  
        }else {
           
            $clientCabang    = DB::table('client_data')->select('id','nama')->paginate(15); 
        }
       
        return  json_encode($clientCabang);
       }

        public function CabangClient(Request $req){
            $jumId = strlen($req->id);
            $jumNama  = strlen($req->nama);
            $nama  = trim($req->nama)  ;
            $id    = trim($req->id);
            if ($jumNama > 0)  { 
                $clientCabang    = DB::table('client_address')->select('id_address','nama_bagian')->where('nama_bagian', 'like', '%' .$nama. '%')->get();  
            }
            else if ($jumId > 0)  { 
                $clientCabang    = DB::table('client_address')->select('id_address','nama_bagian')->where('id_address', 'like', '%' .$id. '%')->get();  
            }else {
               
                $clientCabang    = DB::table('client_address')->select('id_address','nama_bagian')->paginate(15); 
            }
           
            return  json_encode($clientCabang);
    
        }

        public function DetailContract_old($id){ 
            $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
            

            if( $pegawai ==NULL){
                $data = array( 'status'   =>'404');
            }else{
                $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai->id)->orderByDesc('id')->first();
                $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->orderByDesc('id')->first();
                $client_data           = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
                $agama                 = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
                $branchSim             = DB::table('bisnis_network')->where('id', $pegawai_phist->network_id)->first();
                $bisnisGroup           = DB::table('bisnis_grup')->where('id', $pegawai_phist->business_id)->first();
                //  $cabang                = DB::table('client_address')->where('id_address',$pegawai_phist->area_id)->first();
                $contract            = DB::table('emp_contract')->where('parent_id',$pegawai->id)->orderBy('id', 'asc')->first();
                $cabang              = DB::table('client_address')->where('id_address',$contract->area)->first();
                if ($cabang == NULL){
                    $adress_client = "0";
                    $client_branch_id = "0";
                    $client_branch_name = "0";
                }else{
                    $adress_client =  $cabang->id_address;
                    $client_branch_id = $cabang->id_client;
                    $client_branch_name = $cabang->nama_bagian;
                }
               
                $typ_contract          = DB::table('mst_data')->where('kodeData',  $pegawai_contract->contract_type)->first();
                if ($pegawai_contract->job == 0){
                    $job_id   = '0';
                    $job_name = '0';
                }else{
                    $job                   = DB::table('rec_job_posisi')->where('id', $pegawai_contract->job)->first();
                    $job_id   = $job->id;
                    $job_name = $job->subject;
                }
               
                $ktpFile               = $pegawai->ktp_filename ; 
                $picFile               = $pegawai->pic_filename ; 
                if ($ktpFile == NULL){$ktpFile = ""; }
          
                $ktpUrl                = "https://sysbpr.simgroup.co.id/intranet//storage/emp/ktp/";
                $ktpUrl                = $ktpUrl . $ktpFile  ;
    
                $picUrl                = "https://sysbpr.simgroup.co.id/intranet//storage/recruit/pic/";
                $picUrl                = $picUrl . $picFile  ;
                $data  =array(
                          'status'                       => '200',
                          'sim_company_id'               => $bisnisGroup->id,
                          'sim_company_name'             => $bisnisGroup->perusahaan,
                          'sim_branch_id'                => $branchSim->id,
                          'sim_branch_name'              => $branchSim->cabang,
                          'client_company_id'            => $client_data->id,
                          'client_company_name'          => $client_data->nama,
                          'client_branch_id'             => $client_branch_id,
                          'client_branch_name'           => $client_branch_name,
                          'job_id'                       => $job_id,
                          'job_name'                     => $job_name,
                          'npo'                          => $pegawai_contract->npo_no,
                          'contract_number'              => $pegawai_contract->sk_no,
                          'contract_file'                => '',
                          'join_date'                    => $pegawai_contract->start_date,
                          'hired_date'                   => $pegawai_contract->end_date,  
                          'contract_start_date'          => $pegawai_contract->start_date,
                          'contract_end_date'            => $pegawai_contract->end_date, 
                          'contract_flow'                => $typ_contract->namaData,
                          'contract_status'              => '',
                        );
            }
          
            


            return  json_encode($data);

        }

        public function CheckStatus($id){
            $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
            $p_ext                  = DB::table('emp_exit_termination')->where('id_pegawai',$pegawai->id)->first();
            if ($p_ext == NULL){
                $data  = array(
                    'tgl_exit' => '',
                    'exit_note' => '',
                    //'status' => '',
                    );
            }else{
                if ($pegawai->status = '197861'){
                    $sts  = 'aktif';
                }
                else{
                    $sts  = 'tdk aktif';
                }
                $data  = array(
                        'tgl_exit' => $p_ext->tanggal_efektif,
                        'exit_note' => $p_ext->keterangan,
                        //'status'    => $sts,
                    );
            }

            return  json_encode($data);
        }

        public function historiContract($id){ 
            $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
            $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai->id)  ->orderByDesc('id')->get();
            $jumlah                = count($pegawai_contract);
            //$jumlah-- ;
            
            // $data  = array(
            //           'sim_company_id'               => $bisnisGroup->id,
            //           'sim_company_name'             => $bisnisGroup->perusahaan,
            //           'sim_branch_id'                => $branchSim->id,
            //           'sim_branch_name'              => $branchSim->cabang,
            //           'client_company_id'            => $client_data->id,
            //           'client_company_name'          => $client_data->nama,
            //           'client_branch_id'             => $cabang->id_address,
            //           'client_branch_name'           => $cabang->nama_bagian,
            //           'job_id'                       => $job->id,
            //           'job_name'                     => $job->subject,
            //           'npo'                          => $pegawai_contract->npo_no,
            //           'contract_number'              => $pegawai_contract->sk_no,
            //           'contract_file'                => '',
            //           'join_date'                    => $pegawai_contract->start_date,
            //           'hired_date'                   => $pegawai_contract->end_date,  
            //           'contract_start_date'          => $pegawai_contract->start_date,
            //           'contract_end_date'            => $pegawai_contract->end_date, 
            //           'contract_flow'                => $typ_contract->namaData,
            //           'contract_status'              => '',
            //         );

            for ($x = 0; $x < $jumlah; $x++){
                $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->where('status','1')->first();
                $client_data           = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
                $agama                 = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
                $branchSim             = DB::table('bisnis_network')->where('id', $pegawai_phist->network_id)->first();
                $bisnisGroup           = DB::table('bisnis_grup')->where('id', $pegawai_phist->business_id)->first();
                $cabang                = DB::table('client_address')->where('id_address',$pegawai_phist->area_id)->first();
                $job                   = DB::table('rec_job_posisi')->where('id', $pegawai_contract[$x]->job)->first();
                $typ_contract          = DB::table('mst_data')->where('kodeData',  $pegawai_contract[$x]->contract_type)->first();
                $ktpFile               = $pegawai->ktp_filename ; 
                $picFile               = $pegawai->pic_filename ; 
                if ($ktpFile == NULL){$ktpFile = ""; }
          
                $ktpUrl                = "https://sysbpr.simgroup.co.id/intranet//storage/emp/ktp/";
                $ktpUrl                = $ktpUrl . $ktpFile  ;
    
                $picUrl                = "https://sysbpr.simgroup.co.id/intranet//storage/recruit/pic/";
                $picUrl                = $picUrl . $picFile  ;
                $tambah= array(
                    'sim_company_id'               => $bisnisGroup->id,
                    'sim_company_name'             => $bisnisGroup->perusahaan,
                    'sim_branch_id'                => $branchSim->id,
                    'sim_branch_name'              => $branchSim->cabang,
                    'client_company_id'            => $client_data->id,
                    'client_company_name'          => $client_data->nama,
                    'client_branch_id'             => $cabang->id_address,
                    'client_branch_name'           => $cabang->nama_bagian,
                    'job_id'                       => $job->id,
                    'job_name'                     => $job->subject,
                    'npo'                          => $pegawai_contract[$x]->npo_no,
                    'contract_number'              => $pegawai_contract[$x]->sk_no,
                    'contract_file'                => '',
                    'join_date'                    => $pegawai_contract[$x]->start_date,
                    'hired_date'                   => $pegawai_contract[$x]->end_date,  
                    'contract_start_date'          => $pegawai_contract[$x]->start_date,
                    'contract_end_date'            => $pegawai_contract[$x]->end_date, 
                    'contract_flow'                => $typ_contract->namaData,
                    'contract_status'              => '',
                  );
    
                $data["detail"][] = $tambah;
            }                  
            return  json_encode($data);

        }

    public function Plafon($id){ 
        $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
        $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->orderByDesc('id')->first();
        $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai->id)->orderByDesc('id')->first();

        $grade_id              = $pegawai_phist->grade;
        $bisnis_id             = $pegawai_phist->business_id;
        $network_id            = $pegawai_phist->network_id;
        // $client_id             = $pegawai_phist->client_id;
        $client_id             = $pegawai_contract->client;
        $area_id               = $pegawai_phist->area_id;
        $payroll_id            = $pegawai_phist->payroll_id;

        $marital               = $pegawai->marital ;

        $matrial_id            = "0";
        $posisi_id             = "0";
        $pangkat_id            = "0";

        // hard code 
        if ($id == 'PEG21072193'  or $id == 'PEG21072193' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }  

        if ($id == 'PEG21080965'  or $id == 'PEG21080965' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == 'PEG21100910'  or $id == 'PEG21100910' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }
        //PEG22031241
        if ($id == 'PEG22031241'  or $id == 'PEG22031241' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        ///---------------------------------
        if ($id == 'PEG21080488'  or $id == 'PEG21080488' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == 'PEG21100910'  or $id == 'PEG21100910' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == 'PEG21110574'  or $id == 'PEG22021854' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == 'PEG21091512'  or $id == 'PEG21091512' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == 'PEG21061139'  or $id == 'PEG21061139' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == 'PEG22063352'  or $id == 'PEG22063352' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == '01070739'  or $id == '01070739' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == '01099126'  or $id == '01099126' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == '01108106'  or $id == '01108106' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == '01195793'  or $id == '01195793' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == 'PEG22050807'  or $id == 'PEG22050807' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == '01223184'  or $id == '01223184' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($id == 'PEG22050761'  or $id == 'PEG22050761' ){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }
        // echo $client_id ; 
        // die() ;
        // cari query yang bikin pusing, bangsat nih desain
        if ($client_id == '368'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '186'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '113'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '213'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '214'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '187'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '162'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '252'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '119'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '215'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '337'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '188'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '35'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '189'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '190'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '402'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '216'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '163'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '184'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '174'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '183'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '199'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '200'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '253'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '212'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }

        if ($client_id == '421'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }


        if ($client_id == '163'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }
        if ($client_id == '36'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }
        if ($client_id == '231'){
            $data      = array(
                'benefit'   =>'2000000',
                
            );
            return  json_encode($data);
            exit;
        }
        $crIdjenis    = DB::table('bnf_komponen')->where('idJenis', $payroll_id)->where('resetKomponen','12')->first();
            if ($crIdjenis <> NULL){  
                if($payroll_id <> '0'){
                    $data      = array(
                        'benefit'   =>  $crIdjenis->plafonKomponen,
                       // 'jenis_payrol' => $payroll_id ,
                    );
                    return  json_encode($data);
                    exit;
                }
               
            }else{ 
               
            }
        $crIdClient     = DB::table('bnf_komponen')->where('idClient', $client_id)->orderBy('plafonKomponen')->first();  
            if ($crIdClient <> NULL){
                $data      = array(
                    'benefit'   => $crIdClient->plafonKomponen,
                    //'idClient'  => $client_id,
                );
                return  json_encode($data);
            }else{ 
                
            }

            $crIdjenis    = DB::table('bnf_komponen')->where('idJenis', $payroll_id)->where('idMarital',$marital)->first();
            if ($crIdjenis <> NULL){  
                if($payroll_id <> '0'){
                    $data      = array(
                        'benefit'   =>  $crIdjenis->plafonKomponen,
                       // 'jenis_payrol' => $payroll_id ,
                    );
                    return  json_encode($data);
                    exit;
                }
               
            }else{ 
               
            }    
        ///---------------------------final nya 
      
        $data      = array(
            'benefit'   =>'0',
            
        );
        return  json_encode($data);
    }  

    public function credensialKetinggalan($id){
        
        $tad_baru  = DB::connection('mysql2')->table('tad_baru')->first();
        $tad_akhir = $tad_baru->count ;
        
       // $tad_akhir++
        $pegawai  = DB::table('emp')->select('id','reg_no','name','birth_date','phone_no','cell_no','email')->where('reg_no','>=',$id)->first();
        // echo "<pre>" ;
        // print_r($pegawai);
        // echo "</pre>" ;
        // die();
        if($pegawai==NULL){
            $data      = array(
                'status'   =>'404',
                'message' =>'tidak ada tad baru',
            );
        }else{
            $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->first();
            $client_data           = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
            if($client_data ==NULL){
                $companyId   = '';
                $companyName = '';
            }else{
               
                $companyId   = $client_data->id ;
                $companyName = $client_data->nama ;
            }
            $data      = array(
                'status'   =>'200',
                'message'  =>'ada tad baru',
                'sim_id'   =>$pegawai->reg_no,
                'nama'     =>$pegawai->name,
                'birth_date' =>$pegawai->birth_date,
                'phone1'     =>$pegawai->phone_no,
                'phone2'     =>$pegawai->cell_no,
                'email'      =>$pegawai->email,
                'count'      =>$tad_akhir,
                'client_company_id'            => $companyId,
                'client_company_name'          => $companyName,
            );
        }

        return  json_encode($data);          
    }


    public function Plafon2($id){ 
        $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
        $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->orderByDesc('id')->first();
        $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai->id)->orderByDesc('id')->first();
        
        echo "<pre>";
        print_r($pegawai_contract);
        die();
        $grade_id              = $pegawai_phist->grade;
        $bisnis_id             = $pegawai_phist->business_id;
        $network_id            = $pegawai_phist->network_id;
        // $client_id             = $pegawai_phist->client_id;
        $client_id             = $pegawai_contract->client;
        $area_id               = $pegawai_phist->area_id;
        $payroll_id            = $pegawai_phist->payroll_id;

        $marital               = $pegawai->marital ;

        $matrial_id            = "0";
        $posisi_id             = "0";
        $pangkat_id            = "0";

        // echo $pegawai->id . "-" .$payroll_id . "-" . $marital . "-" . $client_id  ;
        // die() ;
        // cari query yang bikin pusing, bangsat nih desain
       
        $crIdjenis    = DB::table('bnf_komponen')->where('idJenis', $payroll_id)->where('resetKomponen','12')->first();
            if ($crIdjenis <> NULL){  
                if($payroll_id <> '0'){
                    $data      = array(
                        'benefit'   =>  $crIdjenis->plafonKomponen,
                       // 'jenis_payrol' => $payroll_id ,
                    );
                    return  json_encode($data);
                    exit;
                }
               
            }else{ 
               
            }
        $crIdClient     = DB::table('bnf_komponen')->where('idClient', $client_id)->orderBy('plafonKomponen')->first();  
            if ($crIdClient <> NULL){
                $data      = array(
                    'benefit'   => $crIdClient->plafonKomponen,
                    //'idClient'  => $client_id,
                );
                return  json_encode($data);
            }else{ 
                
            }

            $crIdjenis    = DB::table('bnf_komponen')->where('idJenis', $payroll_id)->where('idMarital',$marital)->first();
            if ($crIdjenis <> NULL){  
                if($payroll_id <> '0'){
                    $data      = array(
                        'benefit'   =>  $crIdjenis->plafonKomponen,
                       // 'jenis_payrol' => $payroll_id ,
                    );
                    return  json_encode($data);
                    exit;
                }
               
            }else{ 
               
            }  
            
            if ($client_id == '36'){
                $data      = array(
                    'benefit'   =>'2000000',
                    'dt'        =>'fif',
                    
                );
                return  json_encode($data);
                exit;
            }
        ///---------------------------final nya 
        
        $data      = array(
            'benefit'   =>'0',
            
        );
        return  json_encode($data);
    }  


    public function Plafon3($id){ 
        $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
        $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->first();
        $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai->id)->first();
        
        $grade_id              = $pegawai_phist->grade;
        $bisnis_id             = $pegawai_phist->business_id;
        $network_id            = $pegawai_phist->network_id;
        $client_id             = $pegawai_phist->client_id;
        $area_id               = $pegawai_phist->area_id;
        $payroll_id            = $pegawai_phist->payroll_id;
        $posisi                = $pegawai_contract->job;

        $matrial_id            = "0";
        $posisi_id             = "0";
        $pangkat_id            = "0";
       


        // cari query yang bikin pusing, bangsat nih desain
        $crIdjenis    = DB::table('dta_tunjangan')->where('idJenis', $payroll_id)->first();
            if ($crIdjenis <> NULL){  
                if($payroll_id <> '0'){
                    $data      = array(
                        'payroll_id'   =>  $payroll_id,
                        'idKomponen'   =>  $crIdjenis->idKomponen,
                        'payroll_id'   => $payroll_id,
                        'area_id'      => $area_id,
                        'network_id'   => $network_id,
                        'posisi'       =>  $posisi,
                        'type'         =>  "0" ,
                        'client_id'    => $client_id,
                        'grade_id'     => $grade_id,
                    );
                    return  json_encode($data);
                    exit;
                }
               
            }else{ 
               
            }
        $crIdClient     = DB::table('dta_tunjangan')->where('idClient', $client_id)->first();  
            if ($crIdClient <> NULL){
                $data      = array(
                    'client_id'    => $client_id,
                    'idKomponen'   => $crIdClient->idKomponen,
                    'payroll_id'   => $payroll_id,
                    'area_id'      => $area_id,
                    'network_id'   => $network_id,
                    'posisi'       =>  $posisi,
                    'type'         =>  "1" ,
                    'grade_id'     => $grade_id,
                );
                return  json_encode($data);
            }else{ 
                
            }
        ///---------------------------final nya 
      
        $data      = array(
                    'client_id'    => $client_id,
                    'payroll_id'   => $payroll_id,
                    'area_id'      => $area_id,
                    'network_id'   => $network_id,
                    'posisi'       =>  $posisi,
                    'type'         =>  "22" ,
                    'grade_id'     => $grade_id,
            
        );
        return  json_encode($data);
    }  

    public function Gapok($id){ 
        $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
        $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->where('status','1')->first();
        $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai->id)->where('status','1')->first();
        
        $grade_id              = $pegawai_phist->grade;
        $bisnis_id             = $pegawai_phist->business_id;
        $network_id            = $pegawai_phist->network_id;
        $client_id             = $pegawai_phist->client_id;
        $area_id               = $pegawai_phist->area_id;
        $payroll_id            = $pegawai_phist->payroll_id;
        $posisi                = (isset($pegawai_contract->job) ?$pegawai_contract->job  : null);

        $matrial_id            = "0";
        $posisi_id             = "0";
        $pangkat_id            = "0";
       
        // HARD CODE;
        if ($id == 'PEG21121307'){
            $data   = array(
                'Gapok' => '4000000',
                'type'  => 'hardcode 1',
            );
            return  json_encode($data);
             exit;      
        }
        // die();
        //pegawai organik
        if ($pegawai->type == '5464'){
            $data      = array(
                'Gapok' => '4000000',
                'type'  => 'simulasi organik',
            );
            return  json_encode($data);
            exit;  
        }

        // simulasi 1
        $simulasi1   = DB::table('dta_tunjangan')
                        ->where('idClient',$client_id)
                        ->where('idArea',$area_id)
                        ->where('keteranganTunjangan','Gaji Pokok')
                        ->where('idPosisi',$posisi)->first();
        if ($simulasi1 <> NULL){  
            $data      = array(
                'Gapok' => $simulasi1->nilaiTunjangan,
                'type'  => 'simulasi 1',
            );
            return  json_encode($data);
            exit;  
        }
       // simulasi 2
       $simulasi2   = DB::table('dta_tunjangan')
       ->where('idClient',$client_id)
       ->where('idArea',$area_id)
       ->where('idGrade',$grade_id)->first();
       //hard code 1
        if ($simulasi2 <> NULL){  

            if ($id == 'PEG21051788' && $id == 'PEG21121307'){
                $data   = array(
                    'Gapok' => '4000000',
                    'type'  => 'hardcode 1',
                );
                return  json_encode($data);
                 exit;      
            }

            if ($id == '01196143' && $id == '01228950'){
                $data   = array(
                    'Gapok' => '1150000',
                    'type'  => 'hardcode 2',
                );
                return  json_encode($data);
                 exit;      
            }

            if( $simulasi2->nilaiTunjangan < 1000000){
                $simulasi21   = DB::table('dta_tunjangan')
                               ->where('idClient',$client_id)
                               ->where('idArea',$area_id)
                               ->where('keteranganTunjangan','base salary')
                               ->orderBy('nilaiTunjangan','asc')
                               ->where('idGrade',$grade_id)->first();
                if($simulasi21){
                    $data   = array(
                        
                        'Gapok' => $simulasi21->nilaiTunjangan,
                        'type'  => 'simulasi 2.1',
                    );
                } else{
                    $data   = array(
                        'Gapok' => '1000000',
                        'type'  => 'simulasi 2.1',
                    );
                }              
               
                return  json_encode($data);
                exit;                 
            }

            $data      = array(
           'Gapok' => $simulasi2->nilaiTunjangan,
           'type'  => 'simulasi 2',
           );
           
           return  json_encode($data);
           exit;  
        }

         // simulasi 3
         $simulasi3   = DB::table('dta_tunjangan')
         ->where('idClient',$client_id)
         ->where('idArea',$area_id)
         ->where('idPosisi',$posisi)->first();
        if ($simulasi3 <> NULL){  
            $data      = array(
            'Gapok' => $simulasi3->nilaiTunjangan,
            'type'  => 'simulasi 3',
        );
        return  json_encode($data);
        exit;  
        }

         // simulasi 4
         $simulasi4   = DB::table('dta_tunjangan')
         ->where('idClient',$client_id)
         ->where('idArea',$area_id)
         ->first();
        if ($simulasi4 <> NULL){  
            $data      = array(
            'Gapok' => $simulasi4->nilaiTunjangan,
            'type'  => 'simulasi 4',
        );
        return  json_encode($data);
        exit;  
        }

         //simulasi 5
       $simulasi5   = DB::table('dta_tunjangan')
       ->where('idClient',$client_id)
       ->where('idPosisi',$posisi )
       ->first();
      if ($simulasi5 <> NULL){  
          $data      = array(
          'Gapok' => $simulasi5->nilaiTunjangan,
          'type'  => 'simulasi 5',
      );
      return  json_encode($data);
      exit;  
      }
    
     ///---------------------------final nya 
      
        $data      = array(
            'Gapok' => '0',
            'type'  => 'simulasi un now',
            
        );
        return  json_encode($data);
    } 
    
    
    public function penempatan2($id){
        $pegawai             = DB::table('emp')->where('reg_no',$id)->first();
        $pegawai_client      = DB::table('emp_phist')->where('parent_id',$pegawai->id)->first();
        $client              = DB::table('client_data')->where('id',$pegawai_client->client_id)->first();
        //$cabang              = DB::table('client_address')->where('id_address',$pegawai_client->area_id)->first();
        $contract            = DB::table('emp_contract')->where('parent_id',$pegawai->id)->orderBy('id', 'asc')->first();
        $job                 = DB::table('rec_job_posisi')->where('id',$contract->job)->first();
        $pegawai_exit        = DB::table('emp_exit_termination')->where('id_pegawai',$pegawai->id)->first();
        $contract_his        = DB::table('emp_contract')->select('npo_no','pos_name','job','start_date','end_date')->where('parent_id',$pegawai->id)->orderBy('id', 'asc')->get();
        $cabang              = DB::table('client_address')->where('id_address',$contract->area)->first();

        if($cabang == NULL){
           $cabang              = DB::table('client_address')->where('id_address',$pegawai_client->area_id)->first();
        }

        if ($contract->job == "0"){
            $job_id     ='';
            $job_nama   ='';
        }else{
            $job_id     = $job->id;
            $job_nama   = $job->subject;
        }

        if ($pegawai_exit <> NULL){
            $srk_file            =  $pegawai_exit->srk_generate_file ;
            $skk_file            =  $pegawai_exit->skk_generate_file ;
            $srkPath             = "https://sysbpr.simgroup.co.id/intranet//storage/exit_termination/srk/" .  $srk_file;
            $srkPath             = $srk_file  ;
            $skkPath             = "https://sysbpr.simgroup.co.id/intranet//storage/exit_termination/skk/" . $skk_file;
        }else{
            $srkPath             = "";
            $skkPath             = "";
        }
        $data      = array(
                            'id_klien'   =>$client->id,
                            'nama_klien' =>$client->nama,
                            'id_cabang'  =>$cabang ->id_address,
                            'cabang'     =>$cabang ->nama_bagian,
                            'id_job'     =>$job_id,
                            'nama_job'   =>$job_nama,
                            'join_date'  =>$pegawai->join_date,
                            'start_date' =>$contract->start_date,
                            'end_date'   =>$contract->end_date,
                            'skk'        =>$skkPath,
                            'srk'        =>$srkPath,

                            
                          );
        $jum = count($contract_his);

        for ($i = 0; $i < $jum; $i++){
            $tambah= array('nama_posisi'=> $contract_his[$i]->pos_name,'stat date' => $contract_his[$i]->start_date,'end date' => $contract_his[$i]->end_date );

            $data["detail"][] = $tambah;
        }                  
        return json_encode($data);
    }

    public function DetailContract2($id){ 
        $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
        

        if( $pegawai ==NULL){
            $data = array( 'status'   =>'404');
        }else{
            $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai->id)->orderByDesc('id')->first();
            $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->orderByDesc('id')->first();
            $client_data           = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
            $agama                 = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
            $branchSim             = DB::table('bisnis_network')->where('id', $pegawai_phist->network_id)->first();
            $bisnisGroup           = DB::table('bisnis_grup')->where('id', $pegawai_phist->business_id)->first();
          //  $cabang                = DB::table('client_address')->where('id_address',$pegawai_phist->area_id)->first();
            $contract            = DB::table('emp_contract')->where('parent_id',$pegawai->id)->orderBy('id', 'asc')->first();
            $cabang              = DB::table('client_address')->where('id_address',$contract->area)->first();

            if ($cabang == NULL){
                $adress_client = "";
                $client_branch_id = "";
                $client_branch_name = "";
            }else{
                $adress_client =  $cabang->id_address;
                $client_branch_id = $cabang->id_client;
                $client_branch_name = $cabang->nama_bagian;
            }
           
            $typ_contract          = DB::table('mst_data')->where('kodeData',  $pegawai_contract->contract_type)->first();
            if ($pegawai_contract->job == 0){
                $job_id   = '0';
                $job_name = '0';
            }else{
                $job                   = DB::table('rec_job_posisi')->where('id', $pegawai_contract->job)->first();
                $job_id   = $job->id;
                $job_name = $job->subject;
            }
           
            $ktpFile               = $pegawai->ktp_filename ; 
            $picFile               = $pegawai->pic_filename ; 
            if ($ktpFile == NULL){$ktpFile = ""; }
      
            $ktpUrl                = "https://sysbpr.simgroup.co.id/intranet//storage/emp/ktp/";
            $ktpUrl                = $ktpUrl . $ktpFile  ;

            $picUrl                = "https://sysbpr.simgroup.co.id/intranet//storage/recruit/pic/";
            $picUrl                = $picUrl . $picFile  ;
            $data  =array(
                      'status'                       => '200',
                      'sim_company_id'               => $bisnisGroup->id,
                      'sim_company_name'             => $bisnisGroup->perusahaan,
                      'sim_branch_id'                => $branchSim->id,
                      'sim_branch_name'              => $branchSim->cabang,
                      'client_company_id'            => $client_data->id,
                      'client_company_name'          => $client_data->nama,
                      'client_branch_id'             => $client_branch_id,
                      'client_branch_name'           => $client_branch_name,
                      'job_id'                       => $job_id,
                      'job_name'                     => $job_name,
                      'npo'                          => $pegawai_contract->npo_no,
                      'contract_number'              => $pegawai_contract->sk_no,
                      'contract_file'                => '',
                      'join_date'                    => $pegawai_contract->start_date,
                      'hired_date'                   => $pegawai_contract->end_date,  
                      'contract_start_date'          => $pegawai_contract->start_date,
                      'contract_end_date'            => $pegawai_contract->end_date, 
                      'contract_flow'                => $typ_contract->namaData,
                      'contract_status'              => '',
                    );
        }
      
        


        return  json_encode($data);

    }
    

    public function authPkp(Request $request){
        $user  = $request->user ;
        $pass  = $request->pass ;
        $datauser  = DB::connection('mysql3')->table('users')->where('username',$user)->first();
        $psdb  = $datauser->password;
       
        if (Hash::check($pass,$psdb)){
            $data      = array(
                'status'   =>'200',
                'message'  =>'User And Pass True',
            );
        }else{
            $data      = array(
                'status'   =>'400',
                'message'  =>'User And Pass false',
            );
        }

        return  json_encode($data);
    }

    Public function dataUserPkp(){
        $pegawai = DB::table('emp')->paginate(20);
        $jumlahPage = $pegawai['0'] ;
        $data     = array('SIM' => 'DATA USER SIM');
        for ($x = 0; $x <= 19; $x++) {
            if($pegawai[$x]->id <> "21693" and $pegawai[$x]->id <> "21694" and $pegawai[$x]->id <> "21695" and  $pegawai[$x]->id <> "21696" and $pegawai[$x]->id <> "21697")
            {

                $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai[$x]->id)->orderByDesc('id')->first();
                $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai[$x]->id)->orderByDesc('id')->first();
                $job                   = DB::table('rec_job_posisi')->where('id', $pegawai_contract->job)->first();
                $branchSim             = DB::table('bisnis_network')->where('id', $pegawai_phist->network_id)->first();

                if ($job == null){
                   $jobIdid   =  "";
                }else{
                    $jobIdid   =  $job->id;
                }
           
                $id      =  $pegawai[$x]->id;
                $user    =  $pegawai[$x]->reg_no;
                $name    =  $pegawai[$x]->name;
                $email   =  "";
                $idUser  =  "";
                $jobId   =  $jobIdid ;
                $network_id  =  $branchSim->id;

                $tambah  = array(
                                'Id'         => $id ,
                                'User'       => $user,
                                'name'       => $name ,
                                'email'      => $email,
                                'idUser'     => $idUser,
                                'jobId'      => $jobId,
                                'networkId'  => $network_id,
                );           
                $data["detail"][] = $tambah;
           }
          
        }
        
        return  json_encode($data);
    }

    public function Gawe(Request $request){
        $simid      = $request->simId ;
        $tokengawe  = $request->token ;

        $tanggal  = date('Y-m-d');
        $key      = "swamandirJayaJaya2020";
        $key      = json_encode($key);
        $token    = $tanggal . $key ;
        $tokenSim = sha1($token);

        if($tokengawe == $tokenSim){
            $pegawai               = DB::table('emp')->where('reg_no',$simid)->first();

            // echo  $pegawai ;
            // die();
            $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->first();
            $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai->id)->first();
            
            $grade_id              = $pegawai_phist->grade;
            $bisnis_id             = $pegawai_phist->business_id;
            $network_id            = $pegawai_phist->network_id;
            $client_id             = $pegawai_phist->client_id;
            $area_id               = $pegawai_phist->area_id;
            $payroll_id            = $pegawai_phist->payroll_id;
            $posisi                = $pegawai_contract->job;
    
            $matrial_id            = "0";
            $posisi_id             = "0";
            $pangkat_id            = "0";
           
            //pegawai organik
            if ($pegawai->type == '5464'){
                $data      = array(
                    'Gapok' => '4000000',
                    'type'  => 'simulasi organik',
                    'status'=> '200',
                    'simId' => $simid,
                );
                return  json_encode($data);
                exit;  
            }
    
            // simulasi 1
            $simulasi1   = DB::table('dta_tunjangan')
                            ->where('idClient',$client_id)
                            ->where('idArea',$area_id)
                            ->where('keteranganTunjangan','Gaji Pokok')
                            ->where('idPosisi',$posisi)->first();
            if ($simulasi1 <> NULL){  
                $data      = array(
                    'Gapok' => $simulasi1->nilaiTunjangan,
                    'type'  => 'simulasi 1',
                    'status'=> '200',
                    'simId' => $simid,
                );
                return  json_encode($data);
                exit;  
            }
           // simulasi 2
           $simulasi2   = DB::table('dta_tunjangan')
           ->where('idClient',$client_id)
           ->where('idArea',$area_id)
           ->where('idGrade',$grade_id)->first();
            if ($simulasi2 <> NULL){  
                $data      = array(
               'Gapok' => $simulasi2->nilaiTunjangan,
               'type'  => 'simulasi 2',
               'status'=> '200',
               'simId' => $simid,
               );
               return  json_encode($data);
               exit;  
            }
    
             // simulasi 3
             $simulasi3   = DB::table('dta_tunjangan')
             ->where('idClient',$client_id)
             ->where('idArea',$area_id)
             ->where('idPosisi',$posisi)->first();
            if ($simulasi3 <> NULL){  
                $data      = array(
                'Gapok' => $simulasi3->nilaiTunjangan,
                'type'  => 'simulasi 3',
                'status'=> '200',
                'simId' => $simid,
            );
            return  json_encode($data);
            exit;  
            }
        }else{
            $data      = array(
                'Gapok' => '0',
                'type'  => 'TOKEN INVALID',
                'status'=> '400',
                'simId' => $simid,
            );
            return  json_encode($data);
            exit; 
        }

        $data      = array(
            'Gapok' => '0',
            'type'  => 'simulasi un now',
            'status'=> '400',
            'simId' => $simid,
            
        );
        return  json_encode($data);
        exit; 


    }

    public function Gapokbeta($id){ 
        $pegawai               = DB::table('emp')->where('reg_no',$id)->first();
        $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->where('status','1')->first();
        $pegawai_contract      = DB::table('emp_contract')->where('parent_id',$pegawai->id)->where('status','1')->first();
        
        $grade_id              = $pegawai_phist->grade;
        $bisnis_id             = $pegawai_phist->business_id;
        $network_id            = $pegawai_phist->network_id;
        $client_id             = $pegawai_phist->client_id;
        $area_id               = $pegawai_phist->area_id;
        $payroll_id            = $pegawai_phist->payroll_id;
        // $posisi                = $pegawai_contract->job;
        $posisi                = (isset($pegawai_contract->job) ?$pegawai_contract->job  : null);
        //(isset($bankAccount->account_no) ?$bankAccount->account_no  : null)
        $matrial_id            = "0";
        $posisi_id             = "0";
        $pangkat_id            = "0";
       
        // echo "<pre>" ;
        // print_r($pegawai_phist);
        // print_r($pegawai_contract);
        // die();
        //pegawai organik
        if ($pegawai->type == '5464'){
            $data      = array(
                'Gapok' => '4000000',
                'type'  => 'simulasi organik',
            );
            return  json_encode($data);
            exit;  
        }

        // simulasi 1
        $simulasi1   = DB::table('dta_tunjangan')
                        ->where('idClient',$client_id)
                        ->where('idArea',$area_id)
                        ->where('keteranganTunjangan','Gaji Pokok')
                        ->orderBy('nilaiTunjangan','asc')
                        ->where('idPosisi',$posisi)->first();
        if ($simulasi1 <> NULL){  
            $data      = array(
                'Gapok' => $simulasi1->nilaiTunjangan,
                'type'  => 'simulasi 1',
            );
            return  json_encode($data);
            exit;  
        }
       // simulasi 2
       $simulasi2   = DB::table('dta_tunjangan')
       ->where('idClient',$client_id)
       ->where('idArea',$area_id)
       ->where('idGrade',$grade_id)->first();
        if ($simulasi2 <> NULL){  
            if( $simulasi2->nilaiTunjangan < 1000000){
                $simulasi21   = DB::table('dta_tunjangan')
                               ->where('idClient',$client_id)
                               ->where('idArea',$area_id)
                               ->where('keteranganTunjangan','base salary')
                               ->orderBy('nilaiTunjangan','asc')
                               ->where('idGrade',$grade_id)->first();
                $data   = array(
                                'Gapok' => $simulasi21->nilaiTunjangan,
                                'type'  => 'simulasi 2.1',
                );
                return  json_encode($data);
                exit;                 
            }
            $data      = array(
           'Gapok' => $simulasi2->nilaiTunjangan,
           'type'  => 'simulasi 2',
           );
           return  json_encode($data);
           exit;  
        }

         // simulasi 3
         $simulasi3   = DB::table('dta_tunjangan')
         ->where('idClient',$client_id)
         ->where('idArea',$area_id)
         ->where('idPosisi',$posisi)->first();
        if ($simulasi3 <> NULL){  
            $data      = array(
            'Gapok' => $simulasi3->nilaiTunjangan,
            'type'  => 'simulasi 3',
        );
        return  json_encode($data);
        exit;  
        }

        // simulasi 4
        $simulasi4   = DB::table('dta_tunjangan')
        ->where('idClient',$client_id)
        ->where('idArea',$area_id)
        ->first();
       if ($simulasi4 <> NULL){  
           $data      = array(
           'Gapok' => $simulasi4->nilaiTunjangan,
           'type'  => 'simulasi 4',
       );
       return  json_encode($data);
       exit;  
       }

       //simulasi 5
       $simulasi5   = DB::table('dta_tunjangan')
       ->where('idClient',$client_id)
       ->where('idPosisi',$posisi )
       ->first();
      if ($simulasi5 <> NULL){  
          $data      = array(
          'Gapok' => $simulasi5->nilaiTunjangan,
          'type'  => 'simulasi 5',
      );
      return  json_encode($data);
      exit;  
      }
    
     ///---------------------------final nya 
      
        $data      = array(
            'Gapok' => '0',
            'type'  => 'simulasi un now',
            
        );
        return  json_encode($data);
    } 


    public function datasrk($id){
        $pegawai             = DB::table('emp')->where('reg_no',$id)->first();
        $pegawai_exit        = DB::table('emp_exit_termination')->where('id_pegawai',$pegawai->id)->first();
        if($pegawai_exit){
            $pegawai_exit_det    = DB::table('emp_exit_termination_srk')->where('id_termination',$pegawai_exit->id)->get();
        }else{
            $data      = array(   
                'jumlah'     => 0 ,   
                'skk'        => 'kosong',
              );
            return json_encode($data);
        }
       


        // echo "<pre>";
        // print_r($pegawai);
        // print_r($pegawai_exit);
        // print_r($pegawai_exit_det);
        // die() ;
        // echo $pegawai_exit_det['1']->filename;
        $jum = count($pegawai_exit_det);
        $skk      = $pegawai_exit->skk_generate_file ;
        if   ($skk  == NULL ){
            $skk = "kosong";
        }else {
            $skk = "https://sysbpr.simgroup.co.id/intranet//storage/exit_termination/skk/" . $skk ;
        }
        $data      = array(   
                            'jumlah'     => $jum ,   
                            'skk'        => $skk,
                          );

       
       // die() ; 
        // $jum = count($contract_his);
      
        for ($i = 0; $i < $jum; $i++){
            if ($pegawai_exit_det[$i]->filename == null){
                $srk   = "https://sysbpr.simgroup.co.id/intranet//storage/exit_termination/srk/srk-6031267e1373c1613833854.pdf";
            }else{
                $srk      = $pegawai_exit_det[$i]->filename ;
                $srk = "https://sysbpr.simgroup.co.id/intranet//storage/exit_termination/srk/" . $srk;
            }

            // if ($pegawai_exit['0']->skk_generate_file == null){
            //     $skk   = "https://sysbpr.simgroup.co.id/intranet//storage/exit_termination/skk/srk-6031267e1373c1613833854.pdf";
            // }else{
            //     $skk      = $pegawai_exit['0']->skk_generate_file ;
            //     $skk = "https://sysbpr.simgroup.co.id/intranet//storage/exit_termination/skk/" . $skk;
            // }

            $skk   = "https://sysbpr.simgroup.co.id/intranet//storage/exit_termination/skk/srk-6031267e1373c1613833854.pdf";
            $tambah= array(
                           'srk'=> $srk,
                           'skk'=> $skk,
                        );

            $data["detail"][] = $tambah;
        }                  
        return json_encode($data);
    }

    public function vnInsertPkp(Request $request){
        $isi              = 0 ;
        $idfpk            = $request->idfpk ;
        $bisnis_kategori  = $request->bisnis_kategori ;
        $tanggal          = $request->tanggal ;
        $mulai            = $request->mulai ;
        $selesai          = $request->selesai ;
        $source_of_order  = $request->source_of_order ;
        $keterangan       = $request->keterangan ;
        $client           = $request->client ;
        $area             = $request->area  ;
        $pe_kebutuhan     = $request->pe_kebutuhan ;
        $kunci            = $request->kunci ;
        $judul            = $request->judul ;
        $status_catatan   = $request->status_catatan ;
        $create_by        = $request->create_by ;
        $kunciOrigin      = "joss" ; 
        $sts              =  $request->status ;

        $dtIn    = array(
            'idfpk'               => $idfpk ,
            'bisnis_kategori'     => $bisnis_kategori,
            'tanggal'             => $tanggal,
            'mulai'               => $mulai,
            'selesai'             => $selesai ,
            'source_of_order'     => $source_of_order,
            'keterangan'          => $keterangan,
            'client'              => $client,
            'status_catatan'      => $status_catatan ,
            'area'                => $area,
            'judul'               => $judul,
            'flag'                => '1',
            'create_by'           => $create_by,
            'kunci'               => $kunci,
        );
        $dtIn   = json_encode($dtIn);
        // echo "<pre>" ;
        // print_r($dtIn);
        // die() ; 
        
       // echo "stsc : " . $status_catata        n  ;
        if($kunci <> $kunciOrigin){
            $isi  = "Kunci Tidak Valid";
        }
        $jumlahfpk    =  DB::connection('mysql2')->table('stg_order_fpk')->where('idfpk',$idfpk)->count();
        if ($jumlahfpk  > 0){
            $isi  = "Idfpk Sudah Ada DI Staging";
        }
        //star of required 
        if ($create_by==""){
            $isi  = "create by tidak boleh kosong";
        }

        if ($status_catatan==""){
            $isi  = "status catatan tidak boleh kosong";
        }

        if ($idfpk==""){
            $isi  = "idfpk tidak boleh kosong";
        }

        if ($bisnis_kategori==""){
            $isi  = "bisnis_kategori tidak boleh kosong";
        }

        if ($tanggal==""){
            $isi  = "tanggal tidak boleh kosong";
        }

        if ($mulai==""){
            $isi  = "mulai tidak boleh kosong";
        }

        if ($selesai==""){
            $isi  = "selesai tidak boleh kosong";
        }

        if ($source_of_order==""){
            $isi  = "source_of_order tidak boleh kosong";
        }

        if ($judul==""){
            $isi  = "judul tidak boleh kosong";
        }

        if ($area==""){
            $isi  = "area tidak boleh kosong";
        }

        if ($client==""){
            $isi  = "client tidak boleh kosong";
        }

        if ($pe_kebutuhan==""){
            $isi  = "pe_kebutuhan tidak boleh kosong";
        }

        if ($keterangan==""){
            $isi  = "keterangan tidak boleh kosong";
        }
        //end of required 

        // validasi format date
        $tglDate   = explode("-",$tanggal);
        $tglTahun  = $tglDate[0];
        $tglBulan  = $tglDate[1];
        $tglHari   = $tglDate[2];

        if(strlen($tglTahun) < 4){
            $isi  = "Format Tanggal Tahun Tidak Sesuia (YYYY-mm-dd)";
        }
        if($tglBulan > 12){
            $isi  = "Format Tanggal Tahun Tidak Sesuia (YYYY-mm-dd)";
        }

        $mulaiDate   = explode("-",$mulai);
        $mulaiTahun  = $mulaiDate[0];
        $mulaiBulan  = $mulaiDate[1];
        $mulaiHari   = $mulaiDate[2];
        
        if(strlen($mulaiTahun) < 4){
            $isi  = "Format Mulai Tahun Tidak Sesuia (YYYY-mm-DD)";
        }
        if($mulaiBulan > 12){
            $isi  = "Format Mulai Tahun Tidak Sesuia (YYYY-MM-DD)";
        }

        $selesaiDate   = explode("-",$selesai);
        $selesaiTahun  = $selesaiDate[0];
        $selesaiBulan  = $selesaiDate[1];
        $selesaiHari   = $selesaiDate[2];
        if(strlen($selesaiTahun) < 4){
            $isi  = "Format Selesai Tahun Tidak Sesuia (YYYY-mm-DD)";
        }
        if($selesaiBulan > 12){
            $isi  = "Format Selesai Tahun Tidak Sesuia (YYYY-MM-DD)";
        }

        //insert ke staging
       
        //die() ;
        if ($isi <> 0 ){
            $balikan = array(
                'status' => '400',
                'message' => $isi
               );
               $cdate    = date('Y-m-d h:i');
               
               DB::connection('mysql2')->table('stg_log_fpk')->insert([
                   'cdate'               => $cdate ,
                   'event'               => 'API',
                   'log1'                => $dtIn,
                   'log2'                => $isi,
                   'log3'                => '' ,
                   'ket'                 => 'vnInsertPkp',
               ]);
        }else{
            date_default_timezone_set('Asia/Jakarta');
            $cdate    = date('Y-m-d h:s:i');
            DB::connection('mysql2')->table('stg_order_fpk')->insert([
                'idfpk'               => $idfpk ,
                'bisnis_kategori'     => $bisnis_kategori,
                'tanggal'             => $tanggal,
                'mulai'               => $mulai,
                'selesai'             => $selesai ,
                'source_of_order'     => $source_of_order,
                'keterangan'          => $keterangan,
                'client'              => $client,
                'status_catatan'      => $status_catatan ,
                'area'                => $area,
                'judul'               => $judul,
                'flag'                => '1',
                'create_by'           => $create_by,
                'create_date'         => $cdate ,
                'pe_kebutuhan'        => $pe_kebutuhan,
                'status'              => $sts ,
            ]);
    
            $balikan = array(
                'status' => '200',
                'message' => 'Data Berhasil Disave'
               );
        }
        
        return json_encode($balikan) ;               
       // echo $idfpk ; 
    }

    public function vnGetPkp(Request $request){
        $idfpk       =  $request->idfpk ;
        $pegawai     =  DB::connection('mysql2')->table('stg_order_fpk')->where('idfpk',$idfpk)->first();
        $flag        =  $pegawai->flag;
        if ($flag == '3'){
            $balikan = array(
                'status'     => '200',
                'message'    => 'Nomor So Telah Tersedia',
                'idso'       => $pegawai->id_so,
                'nomorso'    => $pegawai->nomor_so,
                'idsoparent' => $pegawai->id_so_parent,

               );
        }else{
            $balikan = array(
                'status'     => '400',
                'message'    => 'Nomor So Belum Tersedia',
                'idso'       => 'NULL',
                'nomorso'    => 'NULL',
                'idsoparent' => 'NULL',

               );
        }
        return json_encode($balikan) ;   

    }
    
    
    public function vnInsertOrder(Request $request){
        //echo "ya kamu ";
        $isi              = 0 ;
        $idfpk            = $request->idfpk ;
        $id_job_posisi    = $request->id_job_posisi ;
        $id_cabang        = $request->id_cabang ;
        $kuota            = $request->kuota ;
        $dtIn    = array(
            'idfpk'               => $idfpk ,
            'id_job_posisi'       => $id_job_posisi,
            'id_cabang'           => $id_cabang,
            'kuota'               => $kuota,
            
        );

        $dtIn   = json_encode($dtIn);
        // validasi coy 
        $pegawai     =  DB::connection('mysql2')->table('stg_order_fpk')->where('idfpk',$idfpk)->first();
        $clt      =  $pegawai->client;

        $job        = DB::table('rec_job_posisi')->where('id',$id_job_posisi)->count();   
        if ($job == '0'){
            $isi  = "ID JOB POSISI TIDAK ADA DALAM BPR";
        } 

        $client     = DB::table('client_address')->where('id_address',$id_cabang)->count();   
        if ($client == '0'){
            $isi  = "ID CABANG TIDAK ADA DALAM BPR";
        }

        // $client2     = DB::table('client_address')->where('id_address',$id_cabang)->first();
        // $id_clt      = $client2->id_client;

        // if($id_clt <> $clt){
        //     $isi  = "ID CABANG DAN CLIENT TIDAK ADA DALAM BPR";
        // }


        if ($isi <> 0){
             $balikan = array(
            'status' => '400',
            'message' => $isi,
           );
           date_default_timezone_set('Asia/Jakarta');
           $cdate    = date('Y-m-d h:i');
            DB::connection('mysql2')->table('stg_log_fpk')->insert([
                'cdate'               => $cdate ,
                'event'               => 'API',
                'log1'                => $dtIn,
                'log2'                => $isi,
                'log3'                => '' ,
                'ket'                 => 'vnInsertOrder',
               ]);
        }else{
            $cdate    = date('Y-m-d h:s');
             DB::connection('mysql2')->table('stg_order_job_fpk')->insert([
            'idfpk'               => $idfpk ,
            'id_job_posisi'       => $id_job_posisi,
            'id_cabang'           => $id_cabang,
            'kuota'               => $kuota,
            'create_by'           => '5' ,
            'create_on'           => $cdate,
           
            ]);
            $balikan = array(
                'status' => '200',
                'message' => "Data Berhasil Di simpan",
             );
        }

        return json_encode($balikan) ;   
    }

    public function vnFlag(Request $request){
        $idfpk            = $request->idfpk ;
        $flag             = $request->flag ;
        $kunci            = $request->kunci ;

        if ($flag == 'Y'){
            DB::connection('mysql2')->table('stg_order_fpk')->where('idfpk',$idfpk)->update([
                'flag' => 4
            ]);
        }else{
            DB::connection('mysql2')->table('stg_order_fpk')->where('idfpk',$idfpk)->update([
                'flag' => 8
            ]);
        }
        
        $data      = array(
            'status'   =>'200',
            'message' =>'Flag Berhasil DI Update',
        );
        
        return json_encode($data);
        

    }

    public function vnGetRow(Request $request){
        $idfpk            = $request->idfpk ;
        $pegawai_hit      =  DB::connection('mysql2')->table('stg_order_fpk')->where('idfpk',$idfpk)->count();
       
        if ($pegawai_hit == 0 ){
            $isi    = "IDFPK TIDAK DI TEMUKAN";
            $data = array(
                'status' => '400',
                'message' => $isi,
               );
        }else{
            $pegawai          =  DB::connection('mysql2')->table('stg_order_fpk')->where('idfpk',$idfpk)->first();
            $data      = array(
                'status'             => '200',
                'message'            => 'IDFPK DI TEMUKAN',  
                'idfpk'              =>$pegawai->idfpk,
                'bisnis_kategori'    =>$pegawai->bisnis_kategori,
                'tanggal'            =>$pegawai->tanggal,
                'mulai'              =>$pegawai->mulai,
                'selesai'            =>$pegawai->selesai,
                'source_of_order'    =>$pegawai->source_of_order,
                'judul'              =>$pegawai->judul,
                'keterangan'         =>$pegawai->keterangan,
                'client'             =>$pegawai->client,
                'area'               =>$pegawai->area,
                'pe_kebutuhan'       =>$pegawai->pe_kebutuhan,
                'statusDB'           =>$pegawai->status,
                'status_catatan'     =>$pegawai->status_catatan,
                'id_so'              =>$pegawai->id_so,
                'nomor_so'           =>$pegawai->nomor_so,
                'id_so_parent'       =>$pegawai->id_so_parent,
                'flag'               =>$pegawai->flag,
               
            );
        }
        return json_encode($data);

    }

    public function GaweStg($id){ 
        $pegawai_jum          = DB::table('emp')->where('ktp_no',$id)
                                ->where('status','197861')->count();
        if($pegawai_jum <> 0 ){
            $pegawai               = DB::table('emp')->where('ktp_no',$id)
                                     ->where('status','197861')->first();
            $idEmp                 = $pegawai->id ;
            $tgl_join              = $pegawai->join_date ;
            $emp_contact_jum   = DB::table('emp_contract')->where('parent_id',$idEmp)->where('status','t')->count();
            if($emp_contact_jum <> 0 ){
                $emp_contact        = DB::table('emp_contract')->where('parent_id',$idEmp)->where('status','t')->first();
                $job              = $emp_contact->pos_name ; 
                $id_job           = $emp_contact->job ; 
                $client_id        = $emp_contact->client ; 
                $area_id          = $emp_contact->area ; 
                $tgl_hire         = $emp_contact->start_date ;
                $tgl_akhir        = $emp_contact->end_date ;
                //emp phist 
                $emp_phist_jum   = DB::table('emp_phist')->where('parent_id',$idEmp)->where('status','1')->count();
                if($emp_phist_jum <> 0 ){
                    $emp_phist        = DB::table('emp_phist')->where('parent_id',$idEmp)->where('status','1')->first();
                    $network_id       = $emp_phist->network_id ; 
                    $bisnis_network   = DB::table('bisnis_network')->where('id',$network_id)->first();
                    $coveran          = $bisnis_network->cabang ;
                    $client_adress    = DB::table('client_address')->where('id_address',$area_id)->first();
                    $cb_client        = $client_adress->nama_bagian ; 
                }
               


            }else{
                $job               = 0 ;
            }

            
            
    
         


            $data   = array(
                            'status'        => '200',
                            'simid'         => $pegawai->reg_no,
                            'job_posisi'    => $job,
                            'job_id'        => $id_job,
                            'network_id'    => $network_id,
                            'client_id'     => $client_id,
                            'area_id'       => $area_id,
                            'coveran'       => $coveran,
                            'cabang_client' => $cb_client,
                            'tgl_hire'      => $tgl_hire ,
                            'tgl_end'       => $tgl_akhir, 
                            'tgl_join'      => $tgl_join

                            );                       

        }else{
            $data   = array(
                'status'        => '400',
                );     
        }
        

        return json_encode($data);
    }


    public function GetHis(){ 
        $his          =  DB::connection('mysql2')->table('tbl_history')->get();

        return json_encode($his);

    }


    public function vnGetJobId(Request $request){
        $idfpk       =  $request->idfpk ;
        $pegawai     =  DB::connection('mysql2')->table('stg_order_job_fpk')->where('idfpk',$idfpk)->get();
       
        return json_encode($pegawai) ;   

    }
    
    public function CredensialAwalUat(){ 
        
        // $pegawai  = DB::connection('mysql5')->table('emp')->select('id','reg_no','name','birth_date','phone_no','cell_no','email')
        //                             ->where('type','<>','5464')
        //                             ->where('status','197861')
        //                             ->where('reg_no ','PEG22092358')
        //                             ->where('ess_flag',1)->first() ;

        $pegawai  = DB::connection('mysql5')->table('view_emp_ess')->first() ;                            

        
        // echo "<pre>" ;
        // print_r($pegawai) ;

        // die() ;               
        if($pegawai==NULL){
            $data      = array(
                'status'   =>'404',
                'message' =>'tidak ada tad baru 1',
            );
        }else{

            $pegawai_contract      = DB::connection('mysql5')->table('emp_contract')
                                    ->where('parent_id',$pegawai->id)
                                    ->where('status','t')
                                    ->where('app_addition_status','approved')
                                    // ->where('already_printed','1')
                                    // ->where('generate_file','<>','')
                                    ->orderByDesc('id')
                                    ->first();

            if($pegawai_contract == NULL){
                $data      = array(
                    'status'   =>'404',
                    'message' =>'tidak ada tad baru 2',
                );
            } else{
                $pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->where('status','1')->first();
                if($pegawai_phist==NULL){
                    $companyId   = '';
                    $companyName = '';
                }else{
                    $client_data           = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
                    if($client_data ==NULL){
                        $companyId   = '';
                        $companyName = '';
                    }else{
                       
                        $companyId   = $client_data->id ;
                        $companyName = $client_data->nama ;
                    }
                }
               
                $data      = array(
                    'status'                =>'200',
                    'message'               =>'ada tad baru',
                    'sim_id'                =>$pegawai->reg_no,
                    'nama'                  =>$pegawai->name,
                    'birth_date'            =>$pegawai->birth_date,
                    'phone1'                =>$pegawai->phone_no,
                    'phone2'                =>$pegawai->cell_no,
                    'email'                 =>$pegawai->email,
                    'count'                 =>$pegawai->id,
                    'client_company_id'     => $companyId,
                    'client_company_name'   => $companyName,
                );
            }                        
            
        }
        
        // DB::connection('mysql2')->table('tad_baru')->where('id','1')->update([
        //     'count' => $pegawai->id
        // ]);
        return  json_encode($data);         


    }


    public function credensialStatusUat(Request $request){
        $id  = $request->id ;
       // $id  = "PEG22064772";
        $pegawai  = DB::connection('mysql5')->table('emp')->select('id','reg_no','name','birth_date','phone_no','cell_no','email')
                                    ->where('type','<>','5464')
                                    //->where('status','197861')
                                    ->where('id',$id)
                                    ->where('ess_flag',0)->first() ;
        
                                    
        // echo "<pre>" ; 
        // print_r($pegawai);
        // die() ;                            
        
        if ($pegawai==NULL){
            $data      = array(
                'status'   =>'404',
                'message' =>'ID Tidak Ditemukan',
            );
        }else{
            // $idAkhir  = $tad_baru->count;
            // $idAkhir++;
            $data      = array(
                'status'   =>'200',
                'message' =>'Data Berhasil Di update',
                'lastCount'  => '10' ,
            );

            DB::connection('mysql5')->table('emp')->where('id',$id)->update([
                'ess_flag' => '1'
            ]);
        }

        return  json_encode($data);
    }

    public function DetailContract($id){ 
        //$pegawai               = DB::table('emp')->where('reg_no',$id)->first();
        $pegawai               = DB::connection('mysql5')->table('emp')->where('reg_no',$id)->first();
        //DB::connection('mysql5')->table

        if( $pegawai ==NULL){
            $data = array( 'status'   =>'404');
        }else{
            //$pegawai_contract      = DB::table('emp_contract')
            /*$pegawai_contract      = DB::connection('mysql5')->table('emp_contract')
                                    ->where('parent_id',$pegawai->id)
                                    ->where('status','t')
                                    ->where('app_addition_status','approved')
                                    ->where('already_printed','1')
                                    ->where('generate_file','<>','')
                                    ->orderByDesc('id')
                                    ->first();*/
			/*perubahan logic contract tad, fsa, 30-06-2022*/
			$pegawai_contract      = DB::connection('mysql5')->table('emp_contract')
                                    ->where('parent_id',$pegawai->id)
                                    //->where('status','t')
                                    //->where('app_addition_status','approved')
                                    //->where('already_printed','1')
                                    //->where('generate_file','<>','')
                                    ->orderByDesc('id')
                                    ->first();

            //echo "<pre>" ; 
           // print_r($pegawai_contract) ;
              
            
            if ($pegawai_contract){
                  //$pegawai_phist         = DB::table('emp_phist')->where('parent_id',$pegawai->id)->orderByDesc('id')->first();
                  $pegawai_phist           = DB::connection('mysql5')->table('emp_phist')->where('parent_id',$pegawai->id)->where('status','1')->orderByDesc('id')->first();
               
                  if($pegawai_phist==NULL){
                     $data  =array(
                        'status'                       => '200',
                        'sim_company_id'               => '0',
                        'sim_company_name'             => '0',
                        'sim_branch_id'                => '0',
                        'sim_branch_name'              => '0',
                        'client_company_id'            => '0',
                        'client_company_name'          => '0',
                        'client_branch_id'             => '0',
                        'client_branch_name'           => '0',
                        'job_id'                       => '0',
                        'job_name'                     => '0',
                        'npo'                          => $pegawai_contract->npo_no,
                        'contract_number'              => $pegawai_contract->sk_no,
                        'contract_file'                => '',
                        'join_date'                    => $pegawai_contract->start_date,
                        'hired_date'                   => $pegawai_contract->end_date,  
                        'contract_start_date'          => $pegawai_contract->start_date,
                        'contract_end_date'            => $pegawai_contract->end_date, 
                        'contract_flow'                => '0',
                        'contract_status'              => '',
                      );
                  }else{
                    // echo "321" ;
                    // die() ;
                    $client_data           = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
                    $agama                 = DB::table('client_data')->where('id',$pegawai_phist->client_id)->first();
                    $branchSim             = DB::table('bisnis_network')->where('id', $pegawai_phist->network_id)->first();
                    $bisnisGroup           = DB::table('bisnis_grup')->where('id', $pegawai_phist->business_id)->first();
                   // $contract            = DB::table('emp_contract')->where('parent_id',$pegawai->id)->orderBy('id', 'asc')->first();
                    $contract            = DB::connection('mysql5')->table('emp_contract')->where('parent_id',$pegawai->id)->orderBy('id', 'asc')->first();
                    
                    $cabang              = DB::table('client_address')->where('id_address',$pegawai_phist->area_id)->first();
                    if ($cabang == NULL){
                        $adress_client = "0";
                        $client_branch_id = "0";
                        $client_branch_name = "0";
                    }else{
                        $adress_client =  $cabang->id_address;
                        $client_branch_id = $cabang->id_client;
                        $client_branch_name = $cabang->nama_bagian;
                    }
                   
                    $typ_contract          = DB::table('mst_data')->where('kodeData',  $pegawai_contract->contract_type)->first();
                    if ($pegawai_contract->job == 0){
                        $job_id   = '0';
                        $job_name = '0';
                    }else{
                        //$job                   = DB::table('rec_job_posisi')->where('id', $pegawai_contract->job)->first();
                        $job                   = DB::connection('mysql5')->table('rec_job_posisi')->where('id', $pegawai_contract->job)->first();
                        $job_id   = $job->id;
                        $job_name = $job->subject;
                    }
                   
                    $ktpFile               = $pegawai->ktp_filename ; 
                    $picFile               = $pegawai->pic_filename ; 
                    if ($ktpFile == NULL){$ktpFile = ""; }
              
                    $ktpUrl                = "https://sysbpr.simgroup.co.id/intranet//storage/emp/ktp/";
                    $ktpUrl                = $ktpUrl . $ktpFile  ;
        
                    $picUrl                = "https://sysbpr.simgroup.co.id/intranet//storage/recruit/pic/";
                    $picUrl                = $picUrl . $picFile  ;
                    $data  =array(
                        'status'                       => '200',
                        'sim_company_id'               => $bisnisGroup->id,
                        'sim_company_name'             => $bisnisGroup->perusahaan,
                        'sim_branch_id'                => $branchSim->id,
                        'sim_branch_name'              => $branchSim->cabang,
                        'client_company_id'            => $client_data->id,
                        'client_company_name'          => $client_data->nama,
                        'client_branch_id'             => $client_branch_id,
                        'client_branch_name'           => $client_branch_name,
                        'job_id'                       => $job_id,
                        'job_name'                     => $job_name,
                        'npo'                          => $pegawai_contract->npo_no,
                        'contract_number'              => $pegawai_contract->sk_no,
                        'contract_file'                => '',
                        'join_date'                    => $pegawai_contract->start_date,
                        'hired_date'                   => $pegawai_contract->end_date,  
                        'contract_start_date'          => $pegawai_contract->start_date,
                        'contract_end_date'            => $pegawai_contract->end_date, 
                        'contract_flow'                => $typ_contract->namaData,
                        'contract_status'              => '',
                      );
                  }
                  
                 
            }else{



                //---------------------------------------------------------------
                $data  =array(
                    'status'                       => '500',
                    'sim_company_id'               => '',
                    'sim_company_name'             => '',
                    'sim_branch_id'                => '',
                    'sim_branch_name'              =>'',
                    'client_company_id'            => '',
                    'client_company_name'          => '',
                    'client_branch_id'             => '',
                    'client_branch_name'           => '',
                    'job_id'                       => '',
                    'job_name'                     => '',
                    'npo'                          => '',
                    'contract_number'              => '',
                    'contract_file'                => '',
                    'join_date'                    => '',
                    'hired_date'                   => '',  
                    'contract_start_date'          => '',
                    'contract_end_date'            => '',
                    'contract_flow'                => '',
                    'contract_status'              => '',
                  );
            }

           
        }
      
        


        return  json_encode($data);

    }

    public function testcr($id){
        echo "lalal" ;

    }

}
